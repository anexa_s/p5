// Game Parameters
let W = 600;
let H = 600;
let BIRD_X = 50;
let BIRD_R = 20;
let GRAVITY = 0.5;
let TUBE_W = 50;
let MIN_GAP = 100;
let MAX_GAP = 200;
let TUBE_RATE = 150;
let TUBE_RANDOM = 150;
let SPACE = 32;
let IMPULSE = 30;
let UPPER_RES = 0.5;
let LOWER_RES = 0.1;
let AIR_RES = 0.9;
let flappy;
let background_image;

class Bird {
  constructor(x, r) {
    this.pos = createVector(x, height / 2);
    this.vel = createVector(0, 0);
    this.acc = createVector(0, GRAVITY);
    this.r = r;
  }

  applyForce(g) {
    this.acc = createVector(0, g);
    this.vel.add(this.acc);
  }

  update() {
    if(this.pos.y > height) {
      this.pos.y = height;
      this.vel.mult(UPPER_RES);
    }
    if(this.pos.y < 0) {
      this.pos.y = 0;
      this.vel.mult(-LOWER_RES);
    }
    this.pos.add(this.vel);
  }

  collide(tube) {
    let w = tube.top_pos.x;
    if(this.pos.x + this.r > w && this.pos.x + this.r < w + TUBE_W
      || this.pos.x - this.r > w && this.pos.x  - this.r < w + TUBE_W) {
        if(this.pos.y - this.r > tube.top_h && this.pos.y + this.r < height - tube.bottom_h) {
          return false;
        }
        return true;
    }
    return false;

  }

  draw() {
    fill(255);
    noStroke();
    image(flappy, this.pos.x, this.pos.y, 2 * this.r, 2 * this.r);
  }

}

class Tube{
  constructor() {
    let gap = random(MIN_GAP, MAX_GAP);
    let pos = random(gap / 2, height - gap / 2);

    this.top_pos = createVector(width, 0);
    this.top_h = pos - gap / 2;
    this.bottom_h = height - (pos + gap / 2);
    this.bottom_pos = createVector(width, pos + gap / 2);
    this.hit = false;
  }

  update() {
    this.top_pos.x--;
    this.bottom_pos.x--;
  }

  draw() {
    if(this.hit) {
      fill(238,144,144, 200);
      stroke(100,0,0);
      strokeWeight(3);
    }
    else {
      fill(144,238,144, 200);
      stroke(0,100,0);
      strokeWeight(3);
    }
    rect(this.top_pos.x, this.top_pos.y, TUBE_W, this.top_h);
    rect(this.bottom_pos.x, this.bottom_pos.y, TUBE_W, this.bottom_h);

  }
}

class Game {
  constructor() {
    this.bird = new Bird(BIRD_X, BIRD_R);
    this.tubes = [];
    this.tube_rate = TUBE_RATE;
    this.score_counter = 0;
    this.score = 0;

  }

  keyPressed() {
    this.bird.applyForce(-IMPULSE * GRAVITY);
    this.bird.vel.mult(AIR_RES);
    this.bird.update();
  }

  draw() {
    image(background_image, width/2, height/2, width, height);
    stroke(255);
    strokeWeight(1);
    noFill();
    text("Score: " + this.score, 20, 20);
    this.bird.draw();
    for(let tube of this.tubes)
      tube.draw();
  }

  updateTubes() {
    if(frameCount % this.tube_rate == 0) {
      this.tubes.push(new Tube());
    }
    let tubes = [];
    for(let tube of this.tubes) {
      if(tube.top_pos.x >= -(TUBE_W)) {
        tubes.push(tube);
      }
    }
    this.tubes = tubes;
    for(let tube of this.tubes)
      tube.update();

  }

  update() {
    this.updateTubes();
    this.bird.applyForce(GRAVITY);
    this.bird.update();
    this.score_counter = 1;
    for(let tube of this.tubes) {
      if(this.bird.collide(tube)) {
        tube.hit = true;
        this.score_counter = -5;
      }
      else {
        tube.hit = false;
      }
    }
    this.score += this.score_counter;

  }

}

let game;

function preload() {
  flappy = loadImage('data/flappy.png');
  background_image = loadImage('data/background.png');
}

function setup() {
  createCanvas(W, H);
  imageMode(CENTER);
  game = new Game();
}

function draw() {
  game.update();
  game.draw();
}

function keyPressed() {
  if(keyCode == SPACE) {
    game.keyPressed();
  }
}

function touchStarted() {
  game.keyPressed();
}

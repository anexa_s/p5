let g;

class Particle {
  constructor(x, y, velx, vely, exploded, R, G, B) {
    this.pos = createVector(x, y);
    this.vel = createVector(velx, vely);
    this.r = 8;
    this.lifespan = 255;
    this.exploded = exploded;
    this.deathrate = 5;
    this.R = R;
    this.G = G;
    this.B = B;
  }

  isValid() {
    return this.lifespan > 0;
  }

  show() {
    fill(this.R, this.G, this.B, this.lifespan);
    noStroke();
    ellipse(this.pos.x, this.pos.y, 2 * this.r);
  }

  update() {
    if (this.exploded) {
      this.lifespan -= this.deathrate;
    }
    this.vel.add(g);
    this.pos.add(this.vel);
  }
}

class Firework {
  constructor(x, y, velx, vely) {
    this.particle = new Particle(x, y, velx, vely, false, random(0,255), random(0,255), random(0,255));
    this.fireworks = [];
  }

  isValid() {
    if(this.particle.exploded) {
      for(let firework of this.fireworks) {
        if(firework.isValid()) {
          return true;
        }
      }
      return false;
    }
    return true;

  }

  show() {
    if(this.particle.vel.y < 0) {
      this.particle.show();
    }
    else {
      if(!this.particle.exploded) {
        for(let i = 0; i < 100; i++) {
          let speedx = random(1, 2);
          let speedy = random(1, 2);
          let velx = random(-speedx, speedx);
          let vely = random(-speedy, speedy);
          this.fireworks.push(new Particle(this.particle.pos.x, this.particle.pos.y, velx, vely, true, this.particle.R, this.particle.G, this.particle.B));
        }
        this.particle.exploded = true;
      }

    }

    for(let firework of this.fireworks) {
      firework.show();
    }
  }

  update() {
    this.particle.update();

    for(let firework of this.fireworks) {
      firework.update();
    }
  }
}

let fireworks = [];

function setup() {
  createCanvas(800, 800);
  g = createVector(0, 0.2);
}

function mousePressed() {
  fireworks.push(new Firework(random(0, width), height, 0, -random(10, 20)));
}

function draw() {
  background(0);
  for(let firework of fireworks) {
    firework.update();
    firework.show();
  }

  let new_fireworks = [];
  for(let firework of fireworks) {
    if(firework.isValid()) {
      new_fireworks.push(firework);
    }
  }
  fireworks = new_fireworks;
}

class Edge {
    constructor(id, node_a, node_b, balance_a, balance_b) {
        this.id = id;
        this.node_a = node_a;
        this.node_b = node_b;
        this.balance_a = balance_a;
        this.balance_b = balance_b;
    }

    draw() {
        let d = p5.Vector.dist(this.node_a.pos, this.node_b.pos) - this.node_a.r - this.node_b.r;
        let angle = p5.Vector.sub(this.node_b.pos, this.node_a.pos).angleBetween(createVector(1, 0));
        let start = p5.Vector.add(this.node_a.pos, p5.Vector.sub(this.node_b.pos, this.node_a.pos).setMag(this.node_a.r));
        let balance = this.balance_a + this.balance_b;
        push();
        translate(start.x, start.y);
        rotate(-angle);
        let step_a = this.balance_a / balance * d;
        let step_b = this.balance_b / balance * d;
        stroke(this.node_a.color);
        fill(this.node_a.color);
        rect(0, 0, step_a, EDGE_WIDTH);
        text(this.balance_a, step_a / 2, 2 * EDGE_WIDTH);

        stroke(this.node_b.color);
        fill(this.node_b.color);
        rect(step_a, 0, step_b, EDGE_WIDTH);
        text(this.balance_b, step_a + step_b / 2, 2 * EDGE_WIDTH);

        pop();

    }
}
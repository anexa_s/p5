class Graph {
    constructor(n, m) {
        this.n = n;
        this.m = m;
        this.nodes = this.generateRandomNodes(n); 
        this.edges = this.generateRandomEdges(m);
        this.generateAdjancency();
        this.edge_set = this.generateEdgeSet();
    }

    generateRandomNodes(n) {
        let nodes = [];
        for (let i = 0; i < n; i++) {
            let x = random(R, width - R);
            let y = random(R, height - R);

            nodes.push(new Node(i, x, y));
        }
        return nodes;
    }

    generateRandomEdges(m) {
        let edges = [];
        let edgeSet = {};
        let cnt = 0;
        let balance_a = 0;
        let balance_b = 0;
        while(cnt < m) {
            let node_1 = floor(random(0, this.n));
            let node_2 = floor(random(0, this.n));
            if(node_1 != node_2) {
                if([node_1, node_2] in edgeSet) {
                    ;
                }
                else if([node_2, node_1] in edgeSet) {
                    ;
                }
                else {
                    edgeSet[[node_1, node_2]] = true;
                    balance_a = floor(abs(randomGaussian(MEAN, SD)));
                    balance_b = floor(abs(randomGaussian(MEAN, SD)));
                    edges.push(new Edge(cnt, this.nodes[node_1], this.nodes[node_2], balance_a, balance_b));
                    cnt++;
                }

            }

        }
        return edges;
    }

    generateAdjancency() {
        for(let i = 0; i < this.m; i++) {
            this.nodes[this.edges[i].node_a.id].neighbours.push(this.edges[i].node_b);
            this.nodes[this.edges[i].node_b.id].neighbours.push(this.edges[i].node_a);
        }
    }

    generateEdgeSet() {

    }

    draw() {
        background(0);
        if(random(1) > 0.5) {
            let x = floor(random(0, this.n));
            let c = lerp(this.edges[x].balance_a, 0, 0.01);
            this.edges[x].balance_a = floor(c);
        }
        else {
            let x = floor(random(0, this.n));
            let c = lerp(this.edges[x].balance_b, 0, 0.01);
            this.edges[x].balance_b = floor(c);
        }
        for(let edge of this.edges) {
            edge.draw();
        }
        for(let node of this.nodes) {
            node.draw();
        }
    }

    mousePressed(x, y) {
        for(let node of this.nodes) {
            node.mousePressed(x, y);
        }
    }

    mouseDragged(x, y) {
        for(let node of this.nodes) {
            node.mouseDragged(x, y);
        }

    }

}
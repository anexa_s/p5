let g;
function setup() {
  createCanvas(W, H);
  textAlign(CENTER, CENTER);
  g = new Graph(8, 10);
}

function draw() {
  background(220);
  g.draw();
}

function mousePressed() {
  g.mousePressed(mouseX, mouseY);
}

function mouseDragged() {
  g.mouseDragged(mouseX, mouseY);
}
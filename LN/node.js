class Node {
    constructor(id, x, y) {
        this.id = id;
        this.pos = createVector(x, y);
        this.r = R;
        this.color = color(random(100, 255), random(100, 255), random(100, 255));
        this.neighbours = [];
    }

    draw() {

        push();

        fill(red(this.color), green(this.color), blue(this.color));
        stroke(this.color);
        translate(this.pos.x, this.pos.y);
        ellipse(0, 0, 2 * this.r);

        fill(255);
        stroke(255);
        text(this.id, 0, 0);

        pop();

        /*
        let d = 0;
        for(let neighbour of this.neighbours) {
            d = max(d, p5.Vector.dist(this.pos, neighbour.pos));
        }
        noFill();
        stroke(this.color);
        ellipse(this.pos.x, this.pos.y, 2 * d);
        */
    }

    contains(x, y) {
        return dist(this.pos.x, this.pos.y, x, y) <= this.r;
    }

    mousePressed(x, y) {
        if(this.contains(x, y)) {
            this.off_x = x - this.pos.x;
            this.off_y = y - this.pos.y;
        }
    }

    mouseDragged(x, y) {
        if(this.contains(x, y)) {
            this.pos.x = x - this.off_x;
            this.pos.y = y - this.off_y;
        }
    }

}
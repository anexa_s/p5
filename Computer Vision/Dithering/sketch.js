let img;
let dither;
let greenButtonSytle = 'background-color: #4CAF50; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none;font-size: 16px;';
let play = 0;

function index(i, j, img) {
  return 4 * (j + i * img.width); 

}
class Dither {
  constructor(img, k) {
    this.img = img;
    this.k = k;
  }

  dither() {
    loadPixels();
    for(let i = 0; i < this.img.height - 1; i++) {
      for(let j = 1; j < this.img.width - 1; j++) {
        let idx = index(i, j, img);
        let old_r = pixels[idx];
        let old_g = pixels[idx+1];
        let old_b = pixels[idx+2];

        let quant_r = round(this.k * old_r / 255) * (255 / this.k);
        let quant_g = round(this.k * old_g / 255) * (255 / this.k);
        let quant_b = round(this.k * old_b / 255) * (255 / this.k);


        pixels[idx] = quant_r;
        pixels[idx+1] = quant_g;
        pixels[idx+2] = quant_b;

        let err_r = old_r - quant_r;
        let err_g = old_g - quant_g;
        let err_b = old_b - quant_b;

        pixels[index(i, j+1, img)] += err_r * 7/16;
        pixels[index(i, j+1, img)+1] += err_g * 7/16;
        pixels[index(i, j+1, img)+2] += err_b * 7/16;

        pixels[index(i+1, j+1, img)] += err_r * 1/16;
        pixels[index(i+1, j+1, img)+1] += err_g * 1/16;
        pixels[index(i+1, j+1, img)+2] += err_b * 1/16;

        pixels[index(i+1, j, img)] += err_r * 5/16;
        pixels[index(i+1, j, img)+1] += err_g * 5/16;
        pixels[index(i+1, j, img)+2] += err_b * 5/16;

        pixels[index(i+1, j-1, img)] += err_r * 3/16;
        pixels[index(i+1, j-1, img)+1] += err_g * 3/16;
        pixels[index(i+1, j-1, img)+2] += err_b * 3/16;

      }
    }
    updatePixels();
  }

}

function preload() {
  img = loadImage('data/noisy.png');
}

function setup() {
  createCanvas(img.width, img.height);
  background(img);
  dither = new Dither(img, 1);
  createButton('Dither').mousePressed(() => {play = 1 - play;}).style(greenButtonSytle);
}

function draw() {
  if (play) {
    background(img);
    dither.dither();
    dither.k += 0.05;
    if (dither.k > 5)
      dither.k = 1;
  }
}

// Parameters
let CELL_SIZE = 100;
let BUTTON_GAP = 50;
let greenButtonSytle = 'background-color: #4CAF50; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none;font-size: 16px;';




let processor;
let filter;
let img;

function preload() {
  img = loadImage('data/noisy.png');
}

function setup() {
  createCanvas(img.width, img.height);

  createButton('Box Filter').position(img.width, 0).mousePressed(() => {processor = new LinearProcessor(img);filter = new BoxFilter(19);}).style(greenButtonSytle);
  createButton('Impulse Filter').position(img.width, BUTTON_GAP).mousePressed(() => {processor = new LinearProcessor(img);filter = new ImpulseFilter(3);}).style(greenButtonSytle);
  createButton('Fuzzy Filter').position(img.width, 2 * BUTTON_GAP).mousePressed(() => {processor = new LinearProcessor(img);filter = new FuzzyFilter(19);}).style(greenButtonSytle);
  createButton('Custom Filter').position(img.width, 3 * BUTTON_GAP).mousePressed(() => {processor = new LinearProcessor(img);filter = new CustomFilter(new Matrix([[-1,-1,-1],[-1,9,-1],[-1,-1,-1]]));}).style(greenButtonSytle);
  createButton('Bilateral Filter').position(img.width, 4 * BUTTON_GAP).mousePressed(() => {processor = new BilateralProcessor(img);}).style(greenButtonSytle);

  pixelDensity(1);
  filter = new ImpulseFilter(3);
  processor = new LinearProcessor(img);
}

function draw() {
  background(img);
  let name = processor.constructor.name;
  if(name == "BilateralProcessor")
    processor.applyFilter(mouseX, mouseY);
  else if(name == "LinearProcessor")
    processor.applyFilter(mouseX, mouseY, filter);
}


// Matrix Library
class Matrix {
  constructor(mtx) {
    this.mtx = mtx;
  }

  static construct(n, m) {
    // Constructs a matrix of 0s of dimension [n, m]
    let mtx = [];
    for (let i = 0; i < n; i++) {
      let row = [];
      for (let j = 0; j < m; j++) {
        row.push(0);
      }
      mtx.push(row);
    }
    return mtx;
  }

  static copy(mtx) {
    // Takes a Matrix object create a copy
    let mtx_copy = [];
    for(let i = 0; i < mtx.mtx.length; i++) {
      let row = [];
      for(let j = 0; j < mtx.mtx[i].length; j++) {
        row.push(mtx.mtx[i][j]);
      }
      mtx_copy.push(row);
    }
    return new Matrix(mtx_copy);
  }

  static convolute(mtx_c) {
    let mtx = Matrix.copy(mtx_c);
    let n = mtx.mtx.length;
    let m = mtx.mtx[0].length;

    // horizontal flip
    for(let i = 0; i < n; i++) {
      for(let j = 0; j < floor(m / 2); j++) {
        let temp = mtx.mtx[i][j];
        mtx.mtx[i][j] = mtx.mtx[i][m - j - 1];
        mtx.mtx[i][m-j-1] = temp;
      }
    }

    // vertical flip
    for (let j = 0; j < m; j++) {
      for(let i = 0; i < floor(n / 2); i++) {
        let temp = mtx.mtx[i][j];
        mtx.mtx[i][j] = mtx.mtx[n - i - 1][j];
        mtx.mtx[n - i - 1][j] = temp;
      }
    }
    return mtx;
  }
}


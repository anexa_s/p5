
class ImpulseFilter {
  constructor(n) {
    this.mtx = new Matrix(Matrix.construct(n, n));
    let mid = floor(n / 2);
    this.mtx.mtx[mid][mid] = 1;
  }
}

class BoxFilter {
  constructor(n) {
    this.mtx = new Matrix(Matrix.construct(n, n));
    let area = 0;
    for(let i = 1; i < n - 1; i++) {
      for(let j = 1; j < n - 1; j++) {
        this.mtx.mtx[i][j] = 1;
        area ++;
      }
    }
    for(let i = 1; i < n - 1; i++) {
      for(let j = 1; j < n - 1; j++) {
        this.mtx.mtx[i][j] /= area;
      }
    }
  }

}


class FuzzyFilter {
  constructor(n) {
    let sigma = n / (2 * PI);
    this.mtx = new Matrix(Matrix.construct(n, n));
    for(let i = 0; i < n; i++) {
      for(let j = 0; j < n; j++) {
        let x = (i - floor(n/2));
        let y = (j - floor(n/2));
        this.mtx.mtx[i][j] = (1 / (n * sigma)) * (pow(Math.E, ((x * x + y * y) / (-2 * sigma * sigma))));

      }
    }
  }

}

class CustomFilter {
  constructor(mtx) {
    this.mtx = mtx;
  }

}


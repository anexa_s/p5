class LinearProcessor {
  constructor(img) {
    this.img = img;
    this.n = img.height;
    this.m = img.width;
    img.loadPixels();
  }

  applyFilter(x, y, filter) {
    let c = Matrix.convolute(filter.mtx).mtx;
    let n = c.length;

    // constrain box
    const x_start = constrain(x - CELL_SIZE/2, 0, this.m);
    const y_start = constrain(y - CELL_SIZE/2, 0, this.n);
    const x_end = constrain(x + CELL_SIZE/2, 0, this.m);
    const y_end = constrain(y + CELL_SIZE/2, 0, this.n);
    rectMode(CENTER);
    noFill();
    stroke(0);
    strokeWeight(2);
    rect((x_start + x_end) / 2, (y_start + y_end) / 2, x_end - x_start, y_end - y_start);


    // filter application
    loadPixels();
    let offset = floor(n/2);
    for(let i = y_start + offset; i < y_end - offset; i++) {
      for(let j = x_start + offset; j < x_end - offset; j++) {
        let r = 0;
        let g = 0;
        let b = 0;
        for(let dx = -offset; dx <= offset; dx++) {
          for(let dy = -offset; dy <= offset; dy++) {
            let pos_i = i + dy;
            let pos_j = j + dx;
            let idx = 4 * (pos_j + this.m * pos_i);

            r += this.img.pixels[idx] * c[dx + offset][dy + offset];
            g += this.img.pixels[idx + 1] * c[dx + offset][dy + offset];
            b += this.img.pixels[idx + 2] * c[dx + offset][dy + offset];

          }
        }
        let idx = 4 * (j + this.m * i);
        r = constrain(r, 0, 255);
        g = constrain(g, 0, 255);
        b = constrain(b, 0, 255);
        pixels[idx] = r;
        pixels[idx+1] = g;
        pixels[idx+2] = b;
      }
    }
    updatePixels();
  }


}

function G2(x, y, sigma) {
    return (1 / (2 * PI * sigma * sigma)) * (pow(Math.E, ((x * x + y * y) / (-2 * sigma * sigma))));
}

function G1(x, sigma) {
    return (1 / sqrt(2 * PI * sigma * sigma)) * (pow(Math.E, ((x * x) / (-2 * sigma * sigma))));

}

class BilateralProcessor {
  constructor(img) {
    this.img = img;
    this.n = img.height;
    this.m = img.width;
    img.loadPixels();
  }

  applyFilter(x, y) {

    // constrain box
    const x_start = constrain(x - CELL_SIZE/2, 0, this.m);
    const y_start = constrain(y - CELL_SIZE/2, 0, this.n);
    const x_end = constrain(x + CELL_SIZE/2, 0, this.m);
    const y_end = constrain(y + CELL_SIZE/2, 0, this.n);
    rectMode(CENTER);
    noFill();
    stroke(0);
    strokeWeight(2);
    rect((x_start + x_end) / 2, (y_start + y_end) / 2, x_end - x_start, y_end - y_start);


    // filter application
    loadPixels();
    let offset = 3;
    for(let i = y_start + offset; i < y_end - offset; i++) {
      for(let j = x_start + offset; j < x_end - offset; j++) {
        let r = 0;
        let g = 0;
        let b = 0;
        let sumR = 0;
        let sumG = 0;
        let sumB = 0;
        let idx = 4 * (j + this.m * i);
        for(let dx = -offset; dx <= offset; dx++) {
          for(let dy = -offset; dy <= offset; dy++) {
            let pos_i = i + dy;
            let pos_j = j + dx;
            let _idx = 4 * (pos_j + this.m * pos_i);

            let factorR = G2(dx, dy, 20) * G1(this.img.pixels[_idx] - this.img.pixels[idx], 25);
            let factorG = G2(dx, dy, 20) * G1(this.img.pixels[_idx + 1] - this.img.pixels[idx + 1], 25);
            let factorB = G2(dx, dy, 20) * G1(this.img.pixels[_idx + 2] - this.img.pixels[idx + 2], 25);
            r += this.img.pixels[_idx] * factorR;
            g += this.img.pixels[_idx + 1] * factorG; 
            b += this.img.pixels[_idx + 2] * factorB; 

            sumR += factorR;
            sumG += factorG;
            sumB += factorB;

          }
        }
        r /= sumR;
        g /= sumG;
        b /= sumB;
        r = constrain(r, 0, 255);
        g = constrain(g, 0, 255);
        b = constrain(b, 0, 255);
        pixels[idx] = r;
        pixels[idx+1] = g;
        pixels[idx+2] = b;
      }
    }
    updatePixels();
  }


}
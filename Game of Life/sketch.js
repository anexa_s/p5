// Constants
CELL_SIZE = 30 
dx = [1, -1, 0, 0, 1, -1, 1, -1]
dy = [0, 0, 1, -1, -1, 1, 1, -1]
PLAYING = 0

class Cell {
  constructor(i, j) {
    this.i = i;
    this.j = j;
    this.alive = false;
  }

  contains(mouse_i, mouse_j) {
    if(mouse_i >= this.i * CELL_SIZE && mouse_i < this.i * CELL_SIZE + CELL_SIZE && 
      mouse_j >= this.j * CELL_SIZE && mouse_j < this.j * CELL_SIZE + CELL_SIZE)
      return true;
    return false;
  }

  switchLifeStatus() {
    if(this.alive)
      this.alive = false;
    else
      this.alive = true;
  }
}

class Grid {
  constructor(rows, cols) {
    this.rows = rows;
    this.cols = cols;

    // Construct the grid
    this.grid = new Array(rows);
    for(let i = 0; i < rows; i++)
      this.grid[i] = new Array(cols);

    this.next_grid = new Array(rows);
    for(let i = 0; i < rows; i++)
      this.next_grid[i] = new Array(cols);

    // Assign cells
    for(let i = 0; i < rows; i++) {
      for(let j = 0; j < cols; j++) {
        this.grid[i][j] = new Cell(i, j);
      }
    }

    for(let i = 0; i < rows; i++) {
      for(let j = 0; j < cols; j++) {
        this.next_grid[i][j] = new Cell(i, j);
      }
    }
    
  }

  next() {

    for(let i = 0; i < this.rows; i++) {
      for(let j = 0; j < this.cols; j++) {
        let cnt = 0;
        cnt = this.countNeighbours(i, j);
        if(this.grid[i][j].alive) {
          if(cnt < 2 || cnt > 3)
            this.next_grid[i][j].switchLifeStatus();
        }
        else {
          if(cnt == 3)
            this.next_grid[i][j].switchLifeStatus();
        }
      }
    }
    let temp;
    temp = this.grid;
    this.grid = this.next_grid;
    this.next_grid = temp;
  }

  countNeighbours(i, j) {
    let cnt = 0;
    for(let k = 0; k < 8; k++) {
      let x = (i + dx[k] + this.rows) % this.rows;
      let y = (j + dy[k] + this.cols) % this.cols;

      if(this.grid[x][y].alive)
        cnt++;

    }
    return cnt;
  }

  draw() {
    // Draw grid cells
    for(let i = 0; i < this.rows; i++) {
      for(let j = 0; j < this.cols; j++) {
        if(this.grid[i][j].alive)
          fill(0);
        else
          fill(255);
        //noStroke();
        rect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE);
      }
    }
  }

  mousePressed(mouse_i, mouse_j) {
    // Switch to the life status
    for(let i = 0; i < this.rows; i++) {
      for(let j = 0; j < this.cols; j++) {
        if(this.grid[i][j].contains(mouse_i, mouse_j)) {
          this.grid[i][j].switchLifeStatus();
        }
      }
    }
  }

}


function getButton(name, positionX, positionY, func) {
    let button;
    button = createButton(name);
    button.position(positionX * CELL_SIZE, positionY * CELL_SIZE);
    button.mousePressed(func);
    return button;
}

class Controls {
  constructor(rows, cols) {
    this.rows = rows;
    this.cols = cols;
    this.g = new Grid(rows, cols);

    this.start = getButton('Start', this.cols, 0, this.toggleGame);
    this.stop = getButton('Stop', this.cols, 1, this.toggleGame)
  }

  toggleGame() {
    PLAYING = 1 - PLAYING;
  }

  draw() {
    this.g.draw();
    if(PLAYING)
      this.g.next();
  }

  mousePressed(mouseY, mouseX) {
    this.g.mousePressed(mouseY, mouseX);
  }
}

let c;

function setup() {
  createCanvas(800, 800);
  frameRate(10);
  c = new Controls(20, 20);
}

function draw() {
  background(255);
  c.draw();
}

function mousePressed() {
  c.mousePressed(mouseY, mouseX);
}

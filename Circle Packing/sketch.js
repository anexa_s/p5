

let bubbles = [];
let growthRate = 10;
let maxAttempts = 100;
let img;

class Bubble {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.r = 1;
    let idx = 4 *(floor(x) + floor(y) * img.width);
    this.color = color(img.pixels[idx], img.pixels[idx+1], img.pixels[idx+2], img.pixels[idx+3]);
    this.growing = true;
  }

  draw() {
    fill(this.color);
    noStroke();
    // noFill();
    // stroke(255);
    ellipse(this.x, this.y, 2 * this.r);
  }

  update(bubbles) {
    if(this.growing) {
      for(let bubble of bubbles) {
        if(bubble != this && dist(bubble.x, bubble.y, this.x, this.y) <= this.r + bubble.r + 1) {
          this.growing = false;
        }
      }
      if(abs(this.x - width) <= this.r + 1 ||
      abs(this.x) <= this.r + 1 ||
      abs(this.y) <= this.r + 1 ||
      abs(this.y - height) <= this.r + 1) {
        this.growing = false;
      }
    }
    if(this.growing)
      this.r++;
  }
}

function generateBubbles(bubbles) {
  for(let i = 0; i < growthRate; i++) {
    let generated = false;
    let attempts = 0;
    while(!generated) {
      if(attempts >= maxAttempts) {
        break;
      }
      let x = random(width);
      let y = random(height);
      let flag = true;
      for(let bubble of bubbles) {
        if(dist(bubble.x, bubble.y, x, y) <=  bubble.r + 1) {
          flag = false;
        }

      }
      if (flag) {
        bubbles.push(new Bubble(x, y));
        generated = true;
      }
      attempts++;
    }
  }
}

function preload() {
  img = loadImage("data/bird.jpg");

}
function setup() {
  createCanvas(img.width, img.height);
  img.loadPixels();
}

function draw() {
  background(0);
  generateBubbles(bubbles);
  for(let bubble of bubbles) {
    bubble.draw();
    bubble.update(bubbles);
  }
}

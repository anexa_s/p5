CELL_SIZE = 30 
dx = [1, -1, 0, 0]
dy = [0, 0, 1, -1]
code = {
  'left': 0,
  'right': 1,
  'up': 2,
  'down': 3
}
PLAYING = 1

class Cell {
  constructor(i, j) {
    this.i = i;
    this.j = j;
    this.walls = [true, true, true, true];
    this.visited = false;
  }

  contains(mouse_i, mouse_j) {
    if(mouse_i >= this.i * CELL_SIZE && mouse_i < this.i * CELL_SIZE + CELL_SIZE && 
      mouse_j >= this.j * CELL_SIZE && mouse_j < this.j * CELL_SIZE + CELL_SIZE)
      return true;
    return false;
  }

  draw() {
    stroke(1);
    if(this.walls[code['up']]) {
      line(this.j * CELL_SIZE, this.i * CELL_SIZE, this.j * CELL_SIZE + CELL_SIZE, this.i * CELL_SIZE);
    }
    if(this.walls[code['left']]) {
      line(this.j * CELL_SIZE, this.i * CELL_SIZE, this.j * CELL_SIZE, this.i * CELL_SIZE + CELL_SIZE);
    }
    if(this.walls[code['right']]) {
      line(this.j * CELL_SIZE + CELL_SIZE, this.i * CELL_SIZE, this.j * CELL_SIZE + CELL_SIZE, this.i * CELL_SIZE + CELL_SIZE);
    }
    if(this.walls[code['down']]) {
      line(this.j * CELL_SIZE, this.i * CELL_SIZE + CELL_SIZE, this.j * CELL_SIZE + CELL_SIZE, this.i * CELL_SIZE + CELL_SIZE);
    }

  }
}

class Grid {
  constructor(rows, cols) {
    this.rows = rows;
    this.cols = cols;
    this.current = 0;
    this.st = [];

    // Construct the grid
    this.grid = new Array(rows);
    for(let i = 0; i < rows; i++)
      this.grid[i] = new Array(cols);

    // Assign cells
    for(let i = 0; i < rows; i++) {
      for(let j = 0; j < cols; j++) {
        this.grid[i][j] = new Cell(i, j);
      }
    }
    this.grid[0][0].visited = true;
  }


  draw() {
    // Draw grid cells
    for(let i = 0; i < this.rows; i++) {
      for(let j = 0; j < this.cols; j++) {
        this.grid[i][j].draw();
        noStroke();
        // Draw the visited rectangle
        if(this.grid[i][j].visited) {
          fill(255, 0, 255, 100);
          rect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE);
        }
      }
    }
    let i = floor(this.current / this.cols);
    let j = this.current % this.cols;
    fill(255, 0, 255, 100);
    rect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE);
  }

  removeWalls(current, next) {
    let i = floor(current / this.cols);
    let j = current % this.cols;
    let x = floor(next / this.cols);
    let y = next % this.cols;

    if(j - y == 1) {
      this.grid[i][j].walls[code['left']] = false;
      this.grid[x][y].walls[code['right']] = false;
    }
    if(j - y == -1) {
      this.grid[i][j].walls[code['right']] = false;
      this.grid[x][y].walls[code['left']] = false;
    }
    if(i - x == -1) {
      this.grid[i][j].walls[code['down']] = false;
      this.grid[x][y].walls[code['up']] = false;
    }
    if(i - x == 1) {
      this.grid[i][j].walls[code['up']] = false;
      this.grid[x][y].walls[code['down']] = false;
    }
  }

  getNeighbour(i, j) {
    let neighbours = [];
    for(let k = 0; k < 4; k++) {
      let x = i + dx[k];
      let y = j + dy[k];
      if(x >= 0 && x < this.rows && y >= 0 && y < this.cols) {
        if(!this.grid[x][y].visited)
          neighbours.push(x * this.rows + y);
      }
    }

    console.log(neighbours);
    if(neighbours.length)
      return neighbours[floor(random(0, neighbours.length))];
    return undefined;
  }

  createMaze() {
    // get the current cell
    let i = floor(this.current / this.cols);
    let j = this.current % this.cols;

    // get a next neighbour
    let next = this.getNeighbour(i, j);

    if(next) {

      this.st.push(this.current);
      this.removeWalls(this.current, next);

      // set the new current
      this.current = next;
    }
    else {
      if(this.st.length) {
        // set the new current
        this.current = this.st[this.st.length - 1];
        this.st.pop();
      }
      else
        return;
    }

    i = floor(this.current / this.cols);
    j = this.current % this.cols;
    this.grid[i][j].visited = true;


  }

  mousePressed(mouse_i, mouse_j) {
    // Switch to the life status
    if (!PLAYING) {
      let flag = true;
      for (let i = 0; i < this.rows; i++) {
        for (let j = 0; j < this.cols; j++) {
          this.grid[i][j].visited = false;
          if (this.grid[i][j].contains(mouse_i, mouse_j)) {
            this.current = (i * this.cols + j);
            this.grid[i][j].visited = true;
            flag = false;
          }
        }
      }
      if(flag) {
        this.current = 0;
        this.grid[0][0].visited = true;
      }
    }
  }

}



class Controls {
  constructor(rows, cols) {
    this.rows = rows;
    this.cols = cols;
    this.g = new Grid(rows, cols);
  }


  draw() {
    this.g.draw();
    if(PLAYING)
      this.g.createMaze();
  }

  mousePressed(mouseY, mouseX) {
    this.g.mousePressed(mouseY, mouseX);
  }
}

let c;

function setup() {
  createCanvas(800, 800);
  frameRate(20);
  c = new Controls(20, 20);
}

function draw() {
  background(255);
  c.draw();
}

function mousePressed() {
  c.mousePressed(mouseY, mouseX);
}

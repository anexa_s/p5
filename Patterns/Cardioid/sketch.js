let total;
let r;
let factor;

function setup() {
  createCanvas(800, 800);
  r = width / 4;
  factor = 2;
}

function getPoint(index) {
  let i = map(index, 0, total, 0, 2 * PI);
  let x = r * cos(i);
  let y = r * sin(i);
  return createVector(x, y);
}

function draw() {
  background(0);
  translate(width/2, height/2);
  //total = map(mouseX, 0, width, 0, 200);
  total = 200;

  // Draw the circle
  noFill();
  stroke(255);
  ellipse(0, 0, r * 2);

  // Draw the points
  for(let i = 0; i < total; i++) {
    let pos = getPoint(i);
    fill(255);
    ellipse(pos.x, pos.y, 3);
  }

  // Draw the lines
  for(let i = 0; i < total; i++) {
    let s = getPoint(i);
    let e = getPoint(factor * i);
    line(s.x, s.y, e.x, e.y);
  }
  factor += 0.01;
}

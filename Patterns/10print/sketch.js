stroke_size = 20;

let x = 0;
let y = 0;

function setup() {
  createCanvas(800, 800);
  background(0);
  stroke(255);
  strokeWeight(2);
}

function draw() {
  if(random(1) < 0.5)
    line(x, y, x + stroke_size, y + stroke_size);
  else
    line(x + stroke_size, y, x, y + stroke_size);
  x += stroke_size;
  if(x > width) {
    x = 0;
    y += stroke_size;
  }
}

let CELL_SIZE = 100;
let R = 46;
let angle = 0;
let rate = 0.05;
let rows = 7;
let cols = 7;

class Oscillator {
  constructor(x, y, f) {
    this.x = x;
    this.y = y;
    this.f = f;
    this.center = createVector(this.x + CELL_SIZE / 2, this.y + CELL_SIZE / 2);
    this.point = createVector(this.center.x + R * cos(this.f * angle), this.center.y + R * sin(this.f * angle));
  }

  draw() {
    noFill();
    strokeWeight(1);
    stroke(255);
    ellipse(this.center.x, this.center.y, 2 * R);


    strokeWeight(8);
    this.point = createVector(this.center.x + R * cos(this.f * angle), this.center.y + R * sin(this.f * angle));
    point(this.point.x, this.point.y);
  }

}


class Curve {
  constructor() {
    this.history = [];
  }

  update(x, y) {
    this.x = x;
    this.y = y;
    this.history.push(createVector(x, y));
  }

  clear() {
    this.history = [];
  }


  draw() {
    noFill();
    stroke(255);
    strokeWeight(1);

    beginShape();
    for(let i = 0; i < this.history.length; i++) {
      vertex(this.history[i].x, this.history[i].y);
    }
    endShape();

    strokeWeight(8);
    point(this.x, this.y);
  }

}

class Grid {
  constructor(rows, cols) {
    this.rows = rows;
    this.cols = cols;
    this.h_os = [null];
    this.v_os = [null];
    this.createOscillators();
    this.curves = this.createCurves();
  }

  createOscillators() {
    for(let j = 1; j < this.cols; j++) {
      this.h_os.push(new Oscillator(j * CELL_SIZE, 0, j));
    }
    for(let i = 1; i < this.rows; i++) {
      this.v_os.push(new Oscillator(0, i * CELL_SIZE, i));
    }
  }

  createCurves() {
    let grid = [[null]];
    for(let i = 1; i < this.rows; i++) {
      let row = [null];
      for(let j = 1; j < this.cols; j++) {
        row.push(new Curve());
      }
      grid.push(row);
    }
    return grid;
  }

  draw() {
    // Draw the grid
    noFill();
    stroke(255);
    strokeWeight(1);
    for(let i = 0; i < this.rows; i++) {
      for(let j = 0; j < this.cols; j++) {
        rect(i * CELL_SIZE, j * CELL_SIZE, CELL_SIZE, CELL_SIZE);
      }
    }

    // Draw Oscillators
    for(let j = 1; j < this.h_os.length; j++) {
      this.h_os[j].draw();
      strokeWeight(2);
      stroke(255, 100);
      line(this.h_os[j].point.x, 0, this.h_os[j].point.x, height);
    }
    for(let i = 1; i < this.v_os.length; i++) {
      this.v_os[i].draw();
      strokeWeight(2);
      stroke(255, 100);
      line(0, this.v_os[i].point.y, width, this.v_os[i].point.y);
    }


    // Draw Curves
    for(let i = 1; i < this.rows; i++) {
      for(let j = 1; j < this.cols; j++) {
        this.curves[i][j].update(this.h_os[j].point.x, this.v_os[i].point.y);
        this.curves[i][j].draw();
      }
    }

    // update Curves
    if(angle >= 2 * PI) {
      for (let i = 1; i < this.rows; i++) {
        for (let j = 1; j < this.cols; j++) {
          this.curves[i][j].clear();
        }
      }
      angle = 0;

    }

  }

}

let g;

function setup() {
  createCanvas(601, 601);
  background(0);
  g = new Grid(rows, cols);
}

function draw() {
  background(0);
  g.draw();
  angle += rate;
}

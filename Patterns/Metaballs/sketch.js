
class Ball {
  constructor(x, y) {
    this.pos = createVector(x, y);
    let speed = random(2, 5);
    this.vel = createVector(random(-speed, speed), random(-speed, speed));
  }

  draw() {
    noFill();
    stroke(255);
    //ellipse(this.pos.x, this.pos.y, 20);
  }
  
  update() {
    this.pos.add(this.vel);
    if(this.pos.x < 0 || this.pos.x > width) {
      this.vel.x *= -1;
    }
    if(this.pos.y < 0 || this.pos.y > width) {
      this.vel.y *= -1;
    }
  }
}

let blobs = [];

function setup() {
  createCanvas(200, 200);
  pixelDensity(1);
}

function mousePressed() {
  blobs.push(new Ball(mouseX, mouseY));
}
function draw() {
  background(0);
  //console.log(frameRate());
  loadPixels();
  for(let i = 0; i < height; i++) {
    for(let j = 0; j < width; j++) {
      let idx = 4 * (i * width + j);
      let sum = 0;
      for(let blob of blobs) {
        sum += 2000 / (dist(blob.pos.x, blob.pos.y, j, i) + 1);
      }
      pixels[idx] = sum;
      pixels[idx + 1] = 0;
      pixels[idx + 2] = 255;
      pixels[idx + 3] = 255;
    }
  }
  updatePixels();

  for(let blob of blobs) {
    blob.update();
    blob.draw();
  }
}

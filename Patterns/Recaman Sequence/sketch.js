let sequence = [];
let numbers = [];
let pos;
let nextPos;
let step;

function setup() {
  createCanvas(800, 800);
  pos = 0;
  nextPos = 0;
  step = 1;
  sequence[pos] = true;
  numbers.push(pos);
}

function calc() {
  nextPos = pos - step;
  if(nextPos < 0 || sequence[nextPos]) {
    nextPos = pos + step;
  }
  sequence[nextPos] = true;
  numbers.push(nextPos);
}

function show() {
  background(0);
  translate(0, height / 2);
  scale(width/step);
  noFill();
  stroke(255);
  for (let i = 0; i < numbers.length - 1; i++) {
    let centre = (numbers[i] + numbers[i+1]) / 2;
    let diameter = numbers[i+1] - numbers[i];
    if (i % 2)
      arc(centre, 0, diameter, diameter, 0, PI);
    else
      arc(centre, 0, diameter, diameter, PI, 0);
  }
}

function update() {
  step++;
  pos = nextPos;
  if(step >= 1000)
    noLoop();
}

function draw() {
  calc();
  show();
  update();
}

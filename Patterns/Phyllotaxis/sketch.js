let phi;
let r;
let n;
let c;
let sz;

function setup() {
  createCanvas(800, 800);
  background(0);
  sz = 16;
  n = 0;
  c = sz;
  angleMode(DEGREES);
}


function draw() {
  translate(width/2, height/2);
  phi = n * 137.5;
  r = c * sqrt(n);
  x = r * sin(phi);
  y = r * cos(phi);
  fill(r % 256, phi % 256, n % 256);
  ellipse(x, y, sz);
  n++;
}

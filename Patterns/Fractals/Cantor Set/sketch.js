let gap;
let offsetx;
let offsety;

function setup() {
  createCanvas(800, 800);
  gap = 10;
  offsetx = width / 8;
  offsety = height / 8;
}

function draw() {
  background(0);
  cantor(0, 3 * width / 4, 0);
}


function cantor(level, length, offset) {
  stroke(255);
  strokeWeight(2);
  let h = offsety + level * gap;
  // 0 to 1/3, 2/3 to 1 

  line(offsetx + offset, h, offsetx + offset + length, h);

  if (length > 1) {
    cantor(level + 1, length / 3, offset + 0);
    cantor(level + 1, length / 3, offset + 2 * length / 3);
  }
  

}

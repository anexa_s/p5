function setup() {
  createCanvas(800, 800);
}

function draw() {
  background(0);
  drawCircle(width/2, height/2, 300);
}

function drawCircle(x, y, d) {
  stroke(255);
  noFill();
  ellipse(x, y, d);
  if(d > 10) {
    drawCircle(x + d/2, y, d/2);
    drawCircle(x - d/2, y, d/2);
    drawCircle(x, y + d/2, d/2);
    drawCircle(x, y - d/2, d/2);
  }

}

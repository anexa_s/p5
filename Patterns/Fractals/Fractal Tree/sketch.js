let angle;
function setup() {
  createCanvas(800, 800);
}

function draw() {
  background(0);
  angleMode(DEGREES);
  angle = map(mouseX, 0, width, 0, 90);
  translate(width / 2, height);
  drawTree(height / 6);
}

function drawTree(length) {
  stroke(255);
  strokeWeight(2);
  line(0, 0, 0, -length);

  translate(0, -length);

  if(length > 3) {
    push();
    rotate(angle);
    drawTree(length * 2 / 3);
    pop();

    push();
    rotate(-angle);
    drawTree(length * 2 / 3);
    pop();

  }

}

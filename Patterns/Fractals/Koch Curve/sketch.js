let lines;

class KochLine {
  constructor(x1, y1, x2, y2) {
    this.start = createVector(x1, y1);
    this.end = createVector(x2, y2);
    this.length = p5.Vector.sub(this.end, this.start).mag();
  }

  lineA() {
    let p1 = this.start;
    let p2 = p5.Vector.add(this.start, p5.Vector.sub(this.end, this.start).setMag(this.length / 3));
    return new KochLine(p1.x, p1.y, p2.x, p2.y);
  }

  lineB() {
    let p1 = p5.Vector.add(this.start, p5.Vector.sub(this.end, this.start).setMag(this.length / 3));
    let p2 = p5.Vector.add(p1, p5.Vector.sub(this.end, this.start).rotate(-60).setMag(this.length / 3));
    return new KochLine(p1.x, p1.y, p2.x, p2.y);

  }

  lineC() {
    let p2 = p5.Vector.add(this.end, p5.Vector.sub(this.start, this.end).setMag(this.length / 3));
    let p1 = p5.Vector.add(p2, p5.Vector.sub(this.start, this.end).rotate(60).setMag(this.length / 3));
    return new KochLine(p1.x, p1.y, p2.x, p2.y);

  }

  lineD() {
    let p1 = p5.Vector.add(this.end, p5.Vector.sub(this.start, this.end).setMag(this.length / 3));
    let p2 = this.end;
    return new KochLine(p1.x, p1.y, p2.x, p2.y);

  }

  draw() {
    stroke(255);
    strokeWeight(2);
    line(this.start.x, this.start.y, this.end.x, this.end.y);
  }

}

function setup() {
  createCanvas(800, 800);
  angleMode(DEGREES);
  lines = [];
  lines.push(new KochLine(0, height / 2, width, height / 2));
}

function generate() {
  newLines = [];
  for(let line of lines) {
    newLines.push(line.lineA());
    newLines.push(line.lineB());
    newLines.push(line.lineC());
    newLines.push(line.lineD());
  }
  lines = newLines;
}

function mousePressed() {
  generate();
}

function draw() {
  background(0);
  for(let line of lines) {
    line.draw();
  }
}

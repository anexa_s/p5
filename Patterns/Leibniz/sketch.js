let step;
let term;
let sum;
let history = [];

function setup() {
  createCanvas(800, 800);
  sum = 0;
  term = 1;
  step = 0;
}

function calc() {
  term =  1 / (2 * step + 1);
  if(step % 2)
    term *= -1;
  sum += term;
  history.push(4 * sum);
  step++;
}

function show() {
  background(0);
  noFill();
  stroke(255);
  let spacing = width / history.length;
  beginShape();
  for(let i = 0; i < history.length; i++) {
    let f = map(history[i], 2, 4, 0, height);
    vertex(i * spacing, f);
  }
  endShape();

  let pi = map(PI, 2, 4, 0, height);
  line(0, pi, width, pi);
}

function draw() {
  calc();
  show();
}

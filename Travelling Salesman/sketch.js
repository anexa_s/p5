class City {
  constructor(x, y) {
    this.pos = createVector(x, y);
  }

  draw() {
    fill(255);
    ellipse(this.pos.x, this.pos.y, 8);
  }

}

class Cities {
  constructor(n) {
    this.cities = [];
    this.n = n;
    for (let i = 0; i < n; i++) {
      this.cities.push(new City(random(width), random(height)));
    }
  }

  draw() {
    // Draw the roads
    stroke(255);
    strokeWeight(2);
    noFill();
    beginShape();
    for(let i = 0; i < this.n; i++) {
      vertex(this.cities[i].pos.x, this.cities[i].pos.y);
    }
    endShape();

    // Draw the cities
    for(let city of this.cities)
      city.draw();
  }

  distance() {
    let sum = 0;
    for(let i = 0; i < this.n - 1; i++) {
      let curr = this.cities[i].pos;
      let next = this.cities[i+1].pos;
      sum += dist(curr.x, curr.y, next.x, next.y);
    }
    return sum;
  }

}

let C;

function setup() {
  createCanvas(800, 800);
  C = new Cities(5);
}

function draw() {
  background(0);
  C.draw();
}

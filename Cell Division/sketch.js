class Cell {
  constructor(x, y) {
    this.pos = createVector(x, y);
    this.r = random(20, 40);
    this.color = color(random(100, 255), 0, random(100, 255), 100);
  }

  draw() {
    fill(this.color);
    noStroke();
    ellipse(this.pos.x, this.pos.y, this.r * 2);
  }

  update() {
    let speed = 3;
    this.pos.x += random(-speed, speed);
    this.pos.y += random(-speed, speed);
  }

}

let cells = [];

function setup() {
  createCanvas(800, 800);
}

function draw() {
  background(0);
  for(let cell of cells) {
    cell.update();
    cell.draw();
  }
}

function mousePressed() {
  cells.push(new Cell(mouseX, mouseY));
}
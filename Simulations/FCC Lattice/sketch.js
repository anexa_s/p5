let CELL_SIZE = 5.26 / 2;
let RADIUS = 1.86;
let kb = 1;
let epsilon = 119.8 * kb;
let sigma = 3.405;
let angleX;
let time_scale = 2500;
let angleY;


class Argon {
  constructor(x, y, z) {
    this.m = 39.948;
    this.pos = createVector(x, y, z);
    this.vel = createVector(0, 0, 0);
    this.acc = createVector(0, 0, 0);
  }

  draw() {
    normalMaterial();
    push();
    translate(this.pos.x, this.pos.y, this.pos.z);
    sphere(RADIUS);
    pop();
  }

  applyForce(other_atom) {
    let r = p5.Vector.dist(this.pos, other_atom.pos);
    let k = 4 * epsilon * (12 * pow(sigma, 12) / pow(r, 14) - 6 * pow(sigma, 6) / pow(r, 8));
    let fx = k * (this.pos.x - other_atom.pos.x);
    let fy = k * (this.pos.y - other_atom.pos.y);
    let fz = k * (this.pos.z - other_atom.pos.z);
    this.acc.x += fx / time_scale;
    this.acc.y += fy / time_scale;
    this.acc.z += fz / time_scale;
  }

  update() {
    this.vel.add(this.acc);

    this.pos.add(this.vel);

    this.acc.mult(0);
  }

}

class FCC {
  constructor(l, b, h) {
    this.l = l;
    this.b = b;
    this.h = h;
    this.grid = this.createGrid();
  }

  createGrid() {
    let grid = [];
    for(let i = 0; i < this.l; i++) {
      let rack = [];
      for(let j = 0; j < this.b; j++) {
        let row = [];
        for(let k = 0; k < this.h; k++) {
          if((i + j + k) % 2)
            row.push(null);
          else
            row.push(new Argon(i * CELL_SIZE, j * CELL_SIZE, k * CELL_SIZE));
        }
        rack.push(row);
      }
      grid.push(rack);
    }
    return grid;
  }

  update() {
    for (let i = 0; i < this.l; i++) {
      for (let j = 0; j < this.b; j++) {
        for (let k = 0; k < this.h; k++) {
          if ((i + j + k) % 2 == 0) {
            this.grid[i][j][k].update();
          }
        }
      }
    }
  }

  applyForces() {
    for(let i = 0; i < this.l; i++) {
      for(let j = 0; j < this.b; j++) {
        for(let k = 0; k < this.h; k++) {
          let atom = this.grid[i][j][k];
          if(atom) {
            for(let o_i = 0; o_i < this.l; o_i++) {
              for(let o_j = 0; o_j < this.b; o_j++) {
                for(let o_k = 0; o_k < this.h; o_k++) {
                  if(i == o_i && j == o_j && k == o_k)
                    continue;
                  let other_atom = this.grid[o_i][o_j][o_k];
                  if(other_atom)
                    atom.applyForce(other_atom);
                }
              }
            }

          }


        }
      }
    }
  }

  draw() {
    for(let i = 0; i < this.l; i++) {
      for(let j = 0; j < this.b; j++) {
        for(let k = 0; k < this.h; k++) {
          if((i + j + k) % 2 == 0) {
            this.grid[i][j][k].draw();
          }
        }
      }
    }
  }



}

let fcc;

function setup() {
  createCanvas(1200, 1200, WEBGL);
  angleX = 0;
  angleY = 0;
  fcc = new FCC(9, 9, 9);
}

function draw() {
  scale(10);
  background(0);
  push();
  rotateX(angleX);
  rotateY(angleY);
  fcc.applyForces();
  fcc.update();
  fcc.draw();
  pop();
  angleX += 0.01;
  angleY += 0.01;
}

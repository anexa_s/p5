let Engine = Matter.Engine;
let World = Matter.World;
let Bodies = Matter.Bodies;

let engine;
let world;


class Box {

  constructor(x, y, w, h) {
    let options = {
      friction: 1,
      restitution: 0.2,
    }
    this.body = Bodies.rectangle(x, y, w, h, options);
    this.w = w;
    this.h = h;
    World.add(world, this.body);
  }

  show() {
    let pos = this.body.position;
    let angle = this.body.angle;

    fill(255, 100);
    stroke(255);

    push();
    translate(pos.x, pos.y);
    rotate(angle);
    rect(0, 0, this.w, this.h);
    pop();

  }
}


class Ground {

  constructor(x, y, w, h) {
    let options = {
      isStatic: true,
    }
    this.body = Bodies.rectangle(x, y, w, h, options);
    this.w = w;
    this.h = h;
    World.add(world, this.body);
  }


  show() {
    fill(255, 100);
    stroke(255);
    strokeWeight(2);
    push();
    let pos = this.body.position;
    translate(pos.x, pos.y);
    rect(0, 0, this.w, this.h);
    pop();
  }

}


let boxes = [];
let ground;

function setup() {
  createCanvas(800, 800);
  engine = Engine.create();
  world = engine.world;
  rectMode(CENTER);


  ground = new Ground(400, height, width, 10);
  Engine.run(engine);
}

function mousePressed() {
  boxes.push(new Box(mouseX, mouseY, 50, 50));
}

function draw() {
  background(0);
  for(let box of boxes)
    box.show();
  ground.show();
}

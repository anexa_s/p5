function setup() {
  createCanvas(800, 800);
}


function draw() {
  background(0);
  translate(width/2, height/2);

  let r = width / 2;
  // Draw the clock base
  noFill();
  stroke(255);
  strokeWeight(6);
  ellipse(0, 0, r);


  // Draw the second arc and hand
  noFill();
  stroke(255, 0, 255);
  strokeWeight(6);
  let s = second();
  let sa = map(s, 0, 60, 0, 2 * PI);
  let so = 20;
  arc(0, 0, r + so, r + so, -HALF_PI, sa - HALF_PI);

  let ss = r /2 - so;
  line(0, 0, ss * cos(sa - HALF_PI), ss * sin(sa - HALF_PI));

  // Draw the minute arc and hand
  noFill();
  strokeWeight(6);
  stroke(255, 0, 0);
  let m = minute();
  let ma = map(m, 0, 60, 0, 2 * PI);
  let mo = so + 20;
  arc(0, 0, r + mo, r + mo, -HALF_PI, ma - HALF_PI);


  let ms = r /2 - mo;
  strokeWeight(8);
  line(0, 0, ms * cos(ma - HALF_PI), ms * sin(ma - HALF_PI));

  // Draw the hour arc and hand
  noFill();
  stroke(255, 255, 0);
  strokeWeight(6);
  let h = hour();
  let ha = map(h % 12, 0, 12, 0, 2 * PI);
  let ho = mo + 20;
  arc(0, 0, r + ho, r + ho, -HALF_PI, ha - HALF_PI);

  let hs = r /2 - ho;
  strokeWeight(10);
  line(0, 0, hs * cos(ha - HALF_PI), hs * sin(ha - HALF_PI));

  fill(255);
  noStroke();
  ellipse(0, 0, 20);

  // Write the time
  noStroke();
  fill(255);
  textSize(45);
  text(h + ':' + m + ':' + s, -80, r/2 + 2 * ho);
  text('CLOCK', -80, -(r/2 + ho));
}

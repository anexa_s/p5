
// Constants
BOMB_COUNT = 20
CELL_SIZE = 30

class Cell {
  constructor(i, j) {
    this.i = i;
    this.j = j;
    this.revealed = false;
    this.bomb = false;
  }

  contains(i, j) {
    if(i >= this.i * CELL_SIZE && i < this.i * CELL_SIZE + CELL_SIZE
      && j >= this.j * CELL_SIZE && j < this.j * CELL_SIZE + CELL_SIZE)
      return true;
    return false;
  }
}

class Grid {
  constructor(rows, cols) {
    // Create a 2D grid
    this.dx = [1, -1, 0, 0, 1, 1, -1, -1];
    this.dy = [0, 0, 1, -1, 1, -1, 1, -1];
    this.rows = rows;
    this.cols = cols;
    this.grid = new Array(rows);
    for(let i = 0; i < rows; i++)
      this.grid[i] = new Array(cols);
    
    // Assign Cells
    for(let i = 0; i < rows; i++) {
      for(let j = 0; j < cols; j++) {
        this.grid[i][j] = new Cell(i, j);
      } 
    }

    // Assign Bombs
    for(let i = 0; i < BOMB_COUNT; i++) {
      let x = int(random(this.rows));
      let y = int(random(this.cols));
      this.grid[x][y].bomb = true;
    }
  }

  draw() {
    // Draw the grid
    for(let i = 0; i < this.rows; i++) {
      for(let j = 0; j < this.cols; j++) {

        // Draw the Cells
        if(this.grid[i][j].revealed)
          fill(255);
        else
          fill(200);
        rect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE)

        // Draw the mines
        if(this.grid[i][j].revealed && this.grid[i][j].bomb)  {
          fill(0);
          ellipse(j * CELL_SIZE + CELL_SIZE / 2, i * CELL_SIZE + CELL_SIZE / 2, CELL_SIZE / 2);
        }
        // Draw the numbers
        else if(this.grid[i][j].revealed) {
          let mineCount = this.checkNeighbours(i, j);
          if(mineCount) {
            textSize(3 * CELL_SIZE / 4);
            fill(0);
            text(mineCount, j * CELL_SIZE + CELL_SIZE / 4, i * CELL_SIZE + 3 * CELL_SIZE / 4);
          }
        }
      }
    }
  }

  checkNeighbours(i, j) {
    let cnt = 0;
    for(let k = 0; k < 8; k++) {
      let x = i + this.dx[k];
      let y = j + this.dy[k];
      if(x >= 0 && x < this.rows && y >= 0 && y < this.cols) {
        if(this.grid[x][y].bomb)
          cnt++;
      }
    }
    return cnt;
  }

  mousePressed(x, y) {
    for(let i = 0; i < this.rows; i++) {
      for(let j = 0; j < this.cols; j++) {
        if(this.grid[i][j].contains(y, x)) {
          this.takeAction(i, j);
          break;
        }
      }
    }
  }

  takeAction(i, j) {
    if(this.grid[i][j].bomb) {
      this.revealAll();
      return;
    }
    this.grid[i][j].revealed = true;
    if(!this.checkNeighbours(i, j)) {
      this.floodFill(i, j);
    }
  }

  floodFill(i, j) {
    this.grid[i][j].revealed = true;
    if(this.checkNeighbours(i, j))
      return;

    for(let k = 0; k < 8; k++) {
      let x = i + this.dx[k];
      let y = j + this.dy[k];
      if(x >= 0 && x < this.rows && y >= 0 && y < this.cols) {
        if(!this.grid[x][y].bomb && !this.grid[x][y].revealed)
          this.floodFill(x, y);
      }
    }
  }

  revealAll() {
    for(let i = 0; i < this.rows; i++) {
      for(let j = 0; j < this.cols; j++) {
        this.grid[i][j].revealed = true;
      }
    }
  }
}


let g;

function setup() {
  createCanvas(800, 800);
  g = new Grid(20, 20)
}

function draw() {
  background(255);
  g.draw()
}

function mousePressed() {
  g.mousePressed(mouseX, mouseY);
}
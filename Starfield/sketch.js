starCount = 1000;

class Star {
  constructor() {
    this.x = random(-width, width);
    this.y = random(-height, height);
    this.z = width;
    this.pz = width;
    this.speed = random(4, 15) + map(this.z, 0, width, 10, 0);
  }

  draw() {
    fill(255);
    noStroke();
    let sx = map(this.x / this.z, 0, 1, 0, width);
    let sy = map(this.y / this.z, 0, 1, 0, height);
    let sz = map(this.z, 0, width, 2, 0);
    ellipse(sx, sy, sz);
    
    if (this.z < this.pz) {
      let px = map(this.x / this.pz, 0, 1, 0, width);
      let py = map(this.y / this.pz, 0, 1, 0, height);
      stroke(255);
      line(sx, sy, px, py);
    }
    
  }

  update() {
    this.pz = this.z;
    this.z -= this.speed;
    if(this.z < this.speed) {
      this.x = random(-width, width);
      this.y = random(-height, height);
      this.z = width;
      this.speed = random(4, 15) + map(this.z, 0, width, 10, 0);
    }
  }
}

let stars = [];
function setup() {
  createCanvas(800, 800);
  for(let i = 0; i < starCount; i++) {
    stars.push(new Star());
  }
}

function draw() {
  background(0);
  translate(width/2, height/2);
  for(let star of stars) {
    star.draw();
    star.update();
  }
}

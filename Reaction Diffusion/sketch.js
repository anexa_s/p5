let H = 300;
let W = 300;
let PIXEL_DENSITY = 1;
let D_A = 1;
let D_B = 0.5;
let FEED = 0.055;
let K = 0.062;
let dt = 1;
let DROP = 5;
let RATE = 5;
class ChemicalCell {
  constructor(chemicals) {
    this.chemicals = chemicals;
  }

}

class Grid {
  constructor() {
    this.n = H;
    this.m = W;
    this.grid = this.createGrid();
    this.nextGrid = this.createGrid();
    this.laplacian = [
      [0.05, 0.2, 0.05],
      [0.2 ,  -1,  0.2],
      [0.05, 0.2, 0.05]
    ];
    
  }

  createGrid() {
    let grid = [];
    for(let i = 0; i < this.n; i++) {
      let row = [];
      for(let j = 0; j < this.m; j++) {
        row.push(new ChemicalCell([1, 0]));
      }
      grid.push(row);
    }
    return grid;
  }

  applyLaplacian(i, j, idx) {
    let sum = 0;
    for(let dx = -1; dx <= 1; dx++) {
      for(let dy = -1; dy <= 1; dy++) {
        let x = i + dx;
        let y = j + dy;
        if(x >= 0 && x < this.n && y >= 0 && y < this.m) {
          sum += this.grid[x][y].chemicals[idx] * this.laplacian[dx+1][dy+1];
        }
      }
    }
    return sum;
  }

  draw() {
    loadPixels();
    for(let i = 0; i < this.n; i++) {
      for(let j = 0; j < this.m; j++) {
        let idx = (j + i * this.m) * 4;
        pixels[idx] = floor(this.grid[i][j].chemicals[0] * 255);
        pixels[idx+1] = 0;
        pixels[idx+2] = floor(this.grid[i][j].chemicals[1] * 255);
        pixels[idx+3] = 255;

      }
    }
    updatePixels();
  }

  update() {
    for (let k = 0; k < RATE; k++) {
      for (let i = 0; i < this.n; i++) {
        for (let j = 0; j < this.m; j++) {
          let a = this.grid[i][j].chemicals[0];
          let b = this.grid[i][j].chemicals[1];
          let na = a + (D_A * this.applyLaplacian(i, j, 0) - a * b * b + FEED * (1 - a)) * dt;
          let nb = b + (D_B * this.applyLaplacian(i, j, 1) + a * b * b - (K + FEED) * b) * dt;
          this.nextGrid[i][j] = new ChemicalCell([na, nb]);
        }
      }
      let temp = this.grid;
      this.grid = this.nextGrid;
      this.nextGrid = temp;
    }

  }

  mousePressed(x, y) {
    // Initial condition
    for (let i = y - DROP; i <= y + DROP; i++) {
      for (let j = x - DROP; j <= x + DROP; j++) {
        this.grid[i][j].chemicals[0] = 0;
        this.grid[i][j].chemicals[1] = 1;
      }
    }

  }
}

let g;

function setup() {
  createCanvas(H, W);
  pixelDensity(PIXEL_DENSITY);
  g = new Grid();
}

function draw() {
  g.draw();
  g.update();
}

function mousePressed() {
  g.mousePressed(mouseX, mouseY);

}